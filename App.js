/*import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <Text>Changes you make will automatically reload.</Text>
        <Text>Shake your phone to open the developer menu.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});*/

import React from 'react';
import { StyleSheet, Button, customStyles, HTML, Modal, TouchableOpacity, Linking, Alert, ImageBackground, Image, Text, View } from 'react-native';
import { ImagePicker } from 'expo';


const remote ='https://i.stack.imgur.com/nkpVq.png';
const rem = 'http://worldartsme.com/images/location-pin-clipart-1.jpg';
const remo = 'https://cdn1.iconfinder.com/data/icons/google_jfk_icons_by_carlosjj/512/chrome.png';
const remon = 'http://2.bp.blogspot.com/-V31y2Ef4Ad0/VZservQf70I/AAAAAAAAdu8/ErI--hbXwfE/s1600/OpenCamera1.png';

export default class App extends React.Component
{
    constructor(props)
    {
      super(props);
      this.state =
      {
        latitude: null,
        longitude: null,
        error: null,
        image: null,

      };
    }

    _pickImage = async () => {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
      });

      console.log(result);

      if (!result.cancelled) {
        this.setState({ image: result.uri });
      }
    };

    componentDidMount()
    {
      navigator.geolocation.getCurrentPosition(
        (position) =>
        {
            this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            error: null,
            });
        },
        (error) => this.setState({ error: error.message }),
        {
            enableHighAccuracy: true, timeout: 20000, maximumAge: 1000
        },
      );
    }

    render()
    {
      let { image } = this.state;

      return (
        <View style={styles.container}>
           <ImageBackground source={{uri: remote}} style={{flex:1}}>
              <Text style={styles.hw}>Hello world!</Text>
              <TouchableOpacity style={styles.button}
                onPress=
                {() =>
                  {
                     Alert.alert('Latitude : ' + this.state.latitude +'\n' +'Longitude : '+this.state.longitude);
                  }
                }
                activeOpacity={0.5}
              >
                <Image
                  source={{uri: rem}}
                  style={styles.imageiconstyle}
                />
                <View style={styles.separatorline}/>
                <Text style={styles.textstyle}>Locate</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.button}
              onPress=
              {()=>
                {
                   Linking.openURL('https://google.com')
                }
              }
              activeOpacity={0.5}
            >
              <Image
                source={{uri: remo}}
                style={styles.imageiconstyle}
              />
              <View style={styles.separatorline}/>
              <Text style={styles.textstyle}>Open Browser</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button}
            onPress={this._pickImage}

            activeOpacity={0.5}
          >
          <Image
            source={{uri: remon}}
            style={styles.imageiconstyle}
          />
          <View style={styles.separatorline}/>
          <Text style={styles.textstyle}>Image Picker</Text>

        </TouchableOpacity>


        <View style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}>
        {image &&
          <Image source={{ uri: image }} style={{width: 200, height: 200}} />}

          {this.state.error ? <Text style={{color:'white',padding:10}}>Error: {this.state.error}</Text> : null}
</View>
        </ImageBackground>
      </View>);
    }
  }

  const styles = StyleSheet.create
  ({
    container:
    {
      flex: 1,
    },

    hw:
    {
      textAlign: 'center',
      color: '#fff',
      fontSize: 50,
      fontWeight: 'bold',
      lineHeight: 48,
      margin: 0,
      paddingTop: 80,
      fontFamily: 'Trocchi',
      fontFamily: 'serif',
    },

    button:
    {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: '#485a96',
      borderWidth: .5,
      borderColor: '#fff',
      height: 40,
      borderRadius: 5 ,
      marginTop: 15,
      marginBottom: 5,
      marginRight: 5,
      width: 200,
      marginLeft: 80,
    },

    imageiconstyle:
    {
      padding: 10,
      margin: 5,
      height: 25,
      width: 25,
      resizeMode : 'stretch',
    },

    textstyle:
    {
      color: "#fff",
      marginBottom : 4,
      marginRight :10,
      marginLeft: 10,
    },

    separatorline:
    {

      backgroundColor : '#fff',
      width: 1,
      height: 40
    }

  });







/*
import React from 'react';
import { StyleSheet, Text, Linking, View, Button, Alert, Div } from 'react-native';

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      error: null,
    };
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  render() {
    return (

      <View>
        <Text style={styles.hw}>Hello world!</Text>

        <Button
          onPress={() => {
          Alert.alert('Latitude:' + this.state.latitude +'\n' +'Longitude:'+this.state.longitude);
          }}
          title="My Geo-Location"
          />

        <Button
          onPress={ ()=>{
          Linking.openURL('https://google.com')
        }}
        title="Open Browser"
        />

        {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
      </View>

    );
  }
}

const styles = StyleSheet.create({

  hw: {
    textAlign: 'center',
    color: '#7c795d',
    fontSize: 50,
    fontWeight: 'bold',
    lineHeight: 48,
    margin: 0,
    paddingTop: 80,
    fontFamily: 'Trocchi',
    fontFamily: 'serif',
  },

  but:{
    padding: 20,
  },

});*/


/*import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <Text>Changes you make will automatically reload.</Text>
        <Text>Shake your phone to open the developer menu.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});*/
